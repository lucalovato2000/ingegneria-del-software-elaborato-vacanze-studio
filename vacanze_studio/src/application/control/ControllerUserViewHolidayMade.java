package application.control;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows you to view the holidays made
 * 
 */
public class ControllerUserViewHolidayMade extends ControllerUserReservedArea {

	String idHoliday;
	@FXML
	private TextField searchusernameTextField;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label nowUsernameLabel;
	@FXML
	private TableView<TableViewHolidays> TableView;
	@FXML
	private TableColumn<TableViewHolidays, String> nameTableView;
	@FXML
	private TableColumn<TableViewHolidays, Date> departureTableView;
	@FXML
	private TableColumn<TableViewHolidays, String> durationTableView;
	@FXML
	private TableColumn<TableViewHolidays, Integer> destinationTableView;
	@FXML
	private TableColumn<TableViewHolidays, String> languageTableView;

	ObservableList<TableViewHolidays> oblistTableView = FXCollections.observableArrayList();

	public ControllerUserViewHolidayMade() {
		super();
	}

	public void createViewHolidayMadeForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_holiday_made_user.fxml"));

			Stage holidaymade = new Stage();

			holidaymade.titleProperty();
			holidaymade.setTitle("Holiday Made");
			holidaymade.initStyle(StageStyle.UNDECORATED);
			holidaymade.setScene(new Scene(root, 536, 560));
			holidaymade.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		super.initialize(arg0, arg1);

		nameTableView.setCellValueFactory(new PropertyValueFactory<>("name"));
		departureTableView.setCellValueFactory(new PropertyValueFactory<>("departure_date"));
		destinationTableView.setCellValueFactory(new PropertyValueFactory<>("destination"));
		durationTableView.setCellValueFactory(new PropertyValueFactory<>("duration"));
		languageTableView.setCellValueFactory(new PropertyValueFactory<>("language"));
		TableView.setItems(oblistTableView);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}

	}

	/**
	 * This method allows view the vacation table
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		if (requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == true
				&& searchusernameTextField.getText().isEmpty()) {
			requiredfieldLabel.setVisible(true);
		} else if (userExists(searchusernameTextField.getText()) == false) {
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("Invalid user!");
			nowUsernameLabel.setText("*");
		} else {
			viewHolidayMade();
		}
	}

	/**
	 * This method populates the vacation view table
	 * 
	 */
	public void viewHolidayMade() {
		String Holiday = null;
		String name = null, destination = null, language = null;
		Date departure_date = null;
		String duration = null;
		char c = '|';

		String idStudent = searchIntoDataBase("id", "student", "username", searchusernameTextField.getText());
		String idHoliday = searchIdHoliday("id_holiday", "survey", "id_student", idStudent);

		for (int i = 0; i < idHoliday.length(); i++) {

			Holiday = searchRecordIntoDataBaseHoliday("*", "holiday", "id", String.valueOf(idHoliday.charAt(i)));

			name = String.valueOf(Holiday.subSequence(0, Holiday.indexOf(c)));

			departure_date = Date.valueOf((String) Holiday.substring(Holiday.indexOf(c) + 1).subSequence(0,
					Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c)));

			duration = String.valueOf(Holiday
					.substring(Holiday.indexOf(c) + 1 + Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1)
					.subSequence(0,
							Holiday.substring(
									Holiday.indexOf(c) + 1 + Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1)
									.indexOf(c)));

			destination = String
					.valueOf(
							Holiday.substring(
									Holiday.indexOf(c) + 1 + Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1
											+ Holiday.substring(Holiday.indexOf(c) + 1
													+ Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1).indexOf(
															c)
											+ 1)
									.subSequence(
											0, Holiday
													.substring(Holiday.indexOf(c) + 1
															+ Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1
															+ Holiday.substring(Holiday.indexOf(c) + 1
																	+ Holiday.substring(Holiday.indexOf(c) + 1)
																			.indexOf(c)
																	+ 1).indexOf(c)
															+ 1)
													.indexOf(c)));

			language = String
					.valueOf(
							Holiday.substring(
									Holiday.indexOf(c) + 1 + Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c) + 1
											+ Holiday
													.substring(Holiday.indexOf(c)
															+ 1 + Holiday.substring(Holiday.indexOf(c) + 1).indexOf(c)
															+ 1)
													.indexOf(c)
											+ 1
											+ Holiday
													.substring(
															Holiday.indexOf(c) + 1
																	+ Holiday
																			.substring(Holiday.indexOf(c) + 1)
																			.indexOf(c)
																	+ 1
																	+ Holiday.substring(Holiday.indexOf(c) + 1
																			+ Holiday.substring(Holiday.indexOf(c) + 1)
																					.indexOf(c)
																			+ 1).indexOf(c)
																	+ 1)
													.indexOf(c)
											+ 1));

			int intDuration = Integer.valueOf(duration);

			oblistTableView.add(new TableViewHolidays(name, departure_date, intDuration, destination, language));
		}
	}

	/**
	 * This class allows you to manage the table TableViewHolidays
	 * 
	 */
	public class TableViewHolidays {

		Integer duration;
		Date departure_date;
		String name, destination, language;

		public TableViewHolidays(String name, Date departure_date, Integer duration, String destination,
				String language) {

			this.name = name;
			this.departure_date = departure_date;
			this.duration = duration;
			this.destination = destination;
			this.language = language;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Date getDeparture_date() {
			return departure_date;
		}

		public void setDeparture_date(Date departure_date) {
			this.departure_date = departure_date;
		}

		public Integer getDuration() {
			return duration;
		}

		public void setDuration(Integer duration) {
			this.duration = duration;
		}

		public String getDestination() {
			return destination;
		}

		public void setDestination(String destination) {
			this.destination = destination;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}
	}

}
