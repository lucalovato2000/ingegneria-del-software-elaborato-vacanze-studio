package application.control;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ControllerSurvey {
	@FXML
	private Button addSurveyButton;
	@FXML
	private ComboBox<Integer> markComboBox;
	@FXML
	private Label markLabel;
	@FXML
	private Label commentLabel;
	@FXML
	private Label studentIdLabel;
	@FXML
	private TextField studentIdTextField;
	@FXML
	private Label holidayIdLabel;
	@FXML
	private TextField holidayIdTextField;
	@FXML
	private Button resetButton;
	@FXML
	private TextArea commentTextArea;
	@FXML
	private Label informationLabel;
	@FXML
	private ImageView backButton;

	/**
	 * Call the method "addSurvey" to test the insert into the database of the
	 * survey
	 * 
	 * @param mark
	 * @param student
	 * @param holiday
	 * @param comment
	 * @return true if the survey is correctly inserted into the databse
	 */
	public static Boolean setValue(Integer mark, String student, String holiday, String comment) {
		return addSurvey(mark, student, holiday, comment);
	}

	private static Boolean addSurvey(Object mark, String studentId, String holidayId, String comment) {
		// Connect to the database
		DBConnect DBInstance = DBConnect.getInstance();
		Connection connection = DBInstance.getConnection();

		// Check that the data inserted by the user are not void
		if (studentId.equals("") || holidayId.equals("") || mark == null) {
			return false;
		} else {
			// Build the query to perform to the database
			String query = "INSERT INTO survey (id_student, id_holiday, grade, comments) ";
			query += "VALUES (" + studentId + ", " + holidayId + ", " + mark + ", '" + comment + "');";

			try {
				// Create the java database statement
				Statement statement = connection.createStatement();

				// Execute the insert query
				statement.execute(query);
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}

		return true;
	}

	/**
	 * Clear all the input fields
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void resetButtonOnAction(ActionEvent event) {
		// Clean the inserted value
		studentIdTextField.setText("");
		holidayIdTextField.setText("");
		commentTextArea.setText("");
		markComboBox.setValue(null);
	}

	/**
	 * Add the information inserted from the user into the database
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void addSurveyButtonOnAction(ActionEvent event) {
		Object mark = markComboBox.getValue();
		String studentId = studentIdTextField.getText();
		String holidayId = holidayIdTextField.getText();
		String comment = commentTextArea.getText();

		// Clean the wrong data alert
		informationLabel.setText("");

		if (addSurvey(mark, studentId, holidayId, comment)) {
			// Set the label color to GREEN and clear the content
			informationLabel.setStyle("-fx-text-fill: #198754; -fx-font-weight: bold; -fx-font-size:12");
			informationLabel.setText("Survey added successfully");

			// Clean the inserted value
			studentIdTextField.setText("");
			holidayIdTextField.setText("");
			commentTextArea.setText("");
			markComboBox.setValue(null);
		} else {
			// Set the label color to RED and clear the content
			informationLabel.setStyle("-fx-text-fill: #dc3545; -fx-font-weight: bold; -fx-font-size:12");
			informationLabel.setText("Please, enter all the required information");
		}
	}

	/**
	 * Go back to the user reserved area
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	void backButtonOnAction(MouseEvent event) throws IOException {
		Main reservedAreaUser = new Main();
		reservedAreaUser.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}
}
