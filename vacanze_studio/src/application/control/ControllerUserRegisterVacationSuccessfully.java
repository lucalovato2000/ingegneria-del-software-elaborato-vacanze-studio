package application.control;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ControllerUserRegisterVacationSuccessfully {

	@FXML
	private Button reservedAreaButton;

	@FXML
	private Button logoutButton;

	public void logoutButtonOnAction(ActionEvent event) throws IOException {
		Main main = new Main();
		main.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}

	public void reservedAreaButtonOnAction(ActionEvent event) throws IOException {
		Main reservedArea = new Main();
		reservedArea.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}
}
