package application.control;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerRegistrationHolidays {

	@FXML
	private Button registerMeButton;
	@FXML
	private Button nextButton;
	@FXML
	private TextField nameTextField;
	@FXML
	private TextField durationTextField;
	@FXML
	private TextField destinationTextField;
	@FXML
	private TextField languageTextField;
	@FXML
	private DatePicker departureDateTextField;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private Label asteriskNameLabel;
	@FXML
	private Label asteriskDepartureDateLabel;
	@FXML
	private Label asteriskDurationLabel;
	@FXML
	private Label asteriskDestinationLabel;
	@FXML
	private Label asteriskLanguageLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmDurationLabel;

	public ControllerRegistrationHolidays() {
	}

	/**
	 * Return the current stage
	 */
	public void createHolidayForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/registration_holidays.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 433));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Move to the next scene
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void registerTripButtonOnAction(ActionEvent event) throws IOException {
		Main newTrip = new Main();
		newTrip.changeSceneWithDimension("/application/view/registration_trip.fxml", 536, 433);
	}

	/**
	 * Cheched if the values in the textfield are valid
	 * 
	 * @param event return to the opening scene
	 */
	public void registerMeButtonOnAction(ActionEvent event) {

		registrationMessageLabel.setText("");

		if (requiredFieldName() == false && requiredFieldDepartureDate() == false && requiredFieldDuration() == false
				&& requestFieldDestination() == false && requestFieldLanguage() == false) {
			if (isNumeric(durationTextField.getText().trim()) == true) {
				confirmDurationLabel.setText("");
				registrationHoliday();
			} else {
				confirmDurationLabel.setText("Please enter only numbers");
			}
		}
	}

	/**
	 * This method allows you to return to the home page
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main adminArea = new Main();
		adminArea.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 536, 560);
	}

	/**
	 * Add a new holiday in the mysql database
	 * 
	 */
	public void registrationHoliday() {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String name = nameTextField.getText();
		LocalDate departureDate = departureDateTextField.getValue();
		String duration = durationTextField.getText();
		String destination = destinationTextField.getText();
		String language = languageTextField.getText();

		if (nameTextField.getText() == null || nameTextField.getText().trim().isEmpty()) {
			name = null;
		}

		if (departureDateTextField.getValue() == null) {
			departureDate = null;
		}

		if (durationTextField.getText() == null || durationTextField.getText().trim().isEmpty()) {
			duration = null;
		}

		if (destinationTextField.getText() == null || destinationTextField.getText().trim().isEmpty()) {
			destination = null;
		}

		if (languageTextField.getText() == null || languageTextField.getText().trim().isEmpty()) {
			language = null;
		}

		// Insert into mysql database the holiday
		String insertFields = " INSERT INTO holiday(name, departure_date, duration, destination, language) VALUES ('";
		String insertValues = name + "','" + departureDate + "','" + duration + "','" + destination + "','" + language
				+ "')";

		String insertToRegister = insertFields + insertValues;

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(insertToRegister);

			registrationMessageLabel.setText("Holiday has been registred successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method checks if the name field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldName() {
		if (nameTextField.getText().trim().isEmpty()) {
			asteriskNameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskNameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the departure_date field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldDepartureDate() {
		if (departureDateTextField.getValue() == null) {
			asteriskDepartureDateLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskDepartureDateLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the duration field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldDuration() {
		if (durationTextField.getText().trim().isEmpty()) {
			asteriskDurationLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskDurationLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the destination field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFieldDestination() {
		if (destinationTextField.getText().trim().isEmpty()) {
			asteriskDestinationLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskDestinationLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the language field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFieldLanguage() {
		if (languageTextField.getText().trim().isEmpty()) {
			asteriskLanguageLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskLanguageLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
