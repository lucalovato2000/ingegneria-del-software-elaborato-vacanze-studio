package application.control;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ControllerAddCollege {
	@FXML
	private Button addCollegeButton;
	@FXML
	private TextField nameTextField;
	@FXML
	private Label wrongDataLabel;
	@FXML
	private TextField addressTextField;
	@FXML
	private Button resetButton;
	@FXML
	private ImageView goBackButton;

	/**
	 * Insert into the database the data of the college
	 * 
	 * @param theName    is the name of the college
	 * @param theAddress is the address of the college
	 * @return true if the college is correctly insert into the database
	 */
	public static Boolean setValue(String collegeName, String collegeAddress) {
		return addCollege(collegeName, collegeAddress);
	}

	/**
	 * Clear all the input text
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void resetButtonOnAction(ActionEvent event) {
		wrongDataLabel.setText("");
		nameTextField.setText("");
		addressTextField.setText("");
	}

	/**
	 * Go back to the admin reserved area
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	void goBackButtonOnAction(MouseEvent event) throws IOException {
		Main adminArea = new Main();
		adminArea.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 536, 560);
	}

	/**
	 * Add the inserted information into the database and clear all the fields
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void addCollegeButtonOnAction(ActionEvent event) {
		String name = nameTextField.getText();
		String address = addressTextField.getText();

		// Clean the wrong data alert
		wrongDataLabel.setText("");

		if (addCollege(name, address)) {
			// Set the label color to GREEN and clear the content
			wrongDataLabel.setStyle("-fx-text-fill: #198754; -fx-font-weight: bold; -fx-font-size:12");
			wrongDataLabel.setText("College added successfully");
			nameTextField.setText("");
			addressTextField.setText("");
		}
		else {
			// Set the label color to RED and clear the content
			wrongDataLabel.setStyle("-fx-text-fill: #dc3545; -fx-font-weight: bold; -fx-font-size:12");
			wrongDataLabel.setText("Please, enter all the required information");
		}
	}

	/**
	 * Add the inserted information into the database
	 * 
	 * @param event return to the opening scene
	 * @return true if the data are correctly inserted into the database, false
	 *         otherwise.
	 */
	static Boolean addCollege(String name, String address) {
		// Connect to the database
		DBConnect DBInstance = DBConnect.getInstance();
		Connection connection = DBInstance.getConnection();

		// Check that the data inserted by the user are not void
		if (name.equals("") || address.equals("")) {
			return false;
		} else {
			// Build the query to perform to the database
			String query = "INSERT INTO college (name, address) VALUES ('" + name + "', '" + address + "');";

			try {
				// Create the java database statement
				Statement statement = connection.createStatement();

				// Execute the insert query
				statement.execute(query);
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}

			return true;
		}
	}
}