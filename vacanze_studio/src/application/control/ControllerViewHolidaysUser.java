package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import application.model.TableHolidays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerViewHolidaysUser implements Initializable {

	@FXML
	private Button nextButton;
	@FXML
	private TableView<TableHolidays> table;
	@FXML
	private TableColumn<TableHolidays, String> col_id;
	@FXML
	private TableColumn<TableHolidays, String> col_name;
	@FXML
	private TableColumn<TableHolidays, Date> col_departure_date;
	@FXML
	private TableColumn<TableHolidays, String> col_destination;
	@FXML
	private TableColumn<TableHolidays, Integer> col_duration;
	@FXML
	private TableColumn<TableHolidays, String> col_language;

	ObservableList<TableHolidays> oblist = FXCollections.observableArrayList();

	public ControllerViewHolidaysUser() {
	}

	/**
	 * Return the current stage
	 */
	public void createViewHolidaysForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_holidays_user.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 600, 422));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Get from the mysql database the data for the instances
	 */
	public void initialize(URL location, ResourceBundle resources) {

		try {

			DBConnect connectNow = DBConnect.getInstance();
			Connection connection = connectNow.getConnection();

			ResultSet rs = connection.createStatement().executeQuery(" SELECT * FROM holiday ");

			while (rs.next()) {
				oblist.add(new TableHolidays(rs.getString("id"), rs.getString("name"), rs.getDate("departure_date"),
						rs.getInt("duration"), rs.getString("destination"), rs.getString("language")));
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
		col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
		col_departure_date.setCellValueFactory(new PropertyValueFactory<>("departure_date"));
		col_destination.setCellValueFactory(new PropertyValueFactory<>("destination"));
		col_duration.setCellValueFactory(new PropertyValueFactory<>("duration"));
		col_language.setCellValueFactory(new PropertyValueFactory<>("language"));

		table.setItems(oblist);

	}

	/**
	 * Return to the previous scene
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main homepage = new Main();
		homepage.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}

	/**
	 * Move to the next scene
	 * 
	 * @param event return to the opening scene
	 */
	public void nextButtonOnAction(ActionEvent event) throws IOException {
		Main holidaybooking = new Main();
		holidaybooking.changeSceneWithDimension("/application/view/holiday_booking.fxml", 520, 550);
	}
}