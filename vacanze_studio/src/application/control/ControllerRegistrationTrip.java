package application.control;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerRegistrationTrip {

	@FXML
	private Button registerMeButton;
	@FXML
	private Button resetButton;
	@FXML
	private TextField destinationTextField;
	@FXML
	private TextField costTextField;
	@FXML
	private TextField hoursTextField;
	@FXML
	private TextField descriptionTextField;
	@FXML
	private TextField idHolidayTextField;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private Label asteriskDestinationLabel;
	@FXML
	private Label asteriskCostLabel;
	@FXML
	private Label asteriskHoursLabel;
	@FXML
	private Label asteriskDescriptionLabel;
	@FXML
	private Label asteriskIDHolidayLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmCostLabel;
	@FXML
	private Label confirmHoursLabel;
	@FXML
	private Label confirmIDHolidayLabel;

	public ControllerRegistrationTrip() {
	}

	/**
	 * Return the current stage
	 */
	public void createTripForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/registration_trip.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 433));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Cheched if the values in the textfield are valid
	 * 
	 * @param event return to the opening scene
	 * @throws NoSuchAlgorithmException
	 */
	public void registerMeButtonOnAction(ActionEvent event) {

		registrationMessageLabel.setText("");

		if (requiredFieldDestination() == false && requiredFieldCost() == false && requestFieldHours() == false
				&& requestFieldDescription() == false) {

			if (isNumeric(costTextField.getText().trim()) == true && isNumeric(hoursTextField.getText().trim()) == true
					&& isNumeric(idHolidayTextField.getText().trim()) == true) {
				confirmCostLabel.setText("");
				confirmHoursLabel.setText("");
				confirmIDHolidayLabel.setText("");
				registrationTrip();
			} else {
				if (isNumeric(costTextField.getText().trim()) == false) {
					confirmCostLabel.setText("Please enter only numbers");
				}
				if (isNumeric(hoursTextField.getText().trim()) == false) {
					confirmHoursLabel.setText("Please enter only numbers");
				}
				if (isNumeric(idHolidayTextField.getText().trim()) == false) {
					confirmIDHolidayLabel.setText("Please enter only numbers");
				}
			}
		}
	}

	/**
	 * This method allows you to return to the home page
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main homepage = new Main();
		homepage.changeSceneWithDimension("/application/view/registration_holidays.fxml", 536, 433);
	}

	/**
	 * Reset the textField in the stage
	 * 
	 * @param event return to the opening scene
	 */
	public void resetButtonOnAction() {
		destinationTextField.setText("");
		costTextField.setText("");
		hoursTextField.setText("");
		descriptionTextField.setText("");
		idHolidayTextField.setText("");
	}

	/**
	 * Add a new trip in the mysql database
	 */
	public void registrationTrip() {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String destination = destinationTextField.getText();
		String cost = costTextField.getText();
		String hours = hoursTextField.getText();
		String description = descriptionTextField.getText();
		String IDHoliday = idHolidayTextField.getText();

		if (destinationTextField.getText() == null || destinationTextField.getText().trim().isEmpty()) {
			destination = null;
		}
		if (costTextField.getText() == null || costTextField.getText().trim().isEmpty()) {
			cost = null;
		}
		if (hoursTextField.getText() == null || hoursTextField.getText().trim().isEmpty()) {
			hours = null;
		}
		if (descriptionTextField.getText() == null || descriptionTextField.getText().trim().isEmpty()) {
			description = null;
		}
		if (idHolidayTextField.getText() == null || idHolidayTextField.getText().trim().isEmpty()) {
			IDHoliday = null;
		}

		// Insert into mysql database the trip
		String insertFields = " INSERT INTO trip(destination, cost, hours, description, id_holiday) VALUES ('";
		String insertValues = destination + "','" + cost + "','" + hours + "','" + description + "','" + IDHoliday
				+ "')";

		String insertToRegister = insertFields + insertValues;

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(insertToRegister);

			registrationMessageLabel.setText("Trip has been registred successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method checks if the destination field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldDestination() {
		if (destinationTextField.getText().trim().isEmpty()) {
			asteriskDestinationLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskDestinationLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the cost field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldCost() {
		if (costTextField.getText().trim().isEmpty()) {
			asteriskCostLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskCostLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the hours field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFieldHours() {
		if (hoursTextField.getText().trim().isEmpty()) {
			asteriskHoursLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskHoursLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the description field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFieldDescription() {
		if (descriptionTextField.getText().trim().isEmpty()) {
			asteriskDescriptionLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskDescriptionLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the id_holiday field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestIdHolidayDescription() {
		if (descriptionTextField.getText().trim().isEmpty()) {
			asteriskIDHolidayLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskIDHolidayLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
