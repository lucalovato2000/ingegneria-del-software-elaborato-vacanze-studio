package application;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;

import org.junit.Assert;
import org.junit.Test;

import application.control.ControllerUserQuestionnarie.TableViewQuestionnaire;
import application.model.DBConnect;
import application.model.TableHolidays;
import application.model.TableStudents;

public class TestAll {

	/**
	 * This method test if the methods in TableStudents return the correct outputs
	 */
	@Test
	public void testTableStudents() {

		TableStudents student = null;

		try {

			DBConnect connectNow = DBConnect.getInstance();
			Connection connection = connectNow.getConnection();

			ResultSet rs = connection.createStatement().executeQuery(" SELECT * FROM student WHERE id=1 ");

			if (rs.next()) {
				student = new TableStudents(rs.getString("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("address"), rs.getString("email"), rs.getLong("phone_number"));
			}

			Assert.assertEquals("1", student.getId());
			Assert.assertEquals("Stefano", student.getName());
			Assert.assertEquals("Rossi", student.getSurname());
			Assert.assertEquals("Via Milano 41, Roma", student.getAddress());
			Assert.assertEquals(new BigDecimal("3298447611"), new BigDecimal(student.getPhone_number()));
			Assert.assertEquals("stefano.rossi@gmail.com", student.getEmail());

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method test if the methods in TableHolidays return the correct outputs
	 */
	@Test
	public void testTableHolidays() {

		TableHolidays holiday = null;

		try {

			DBConnect connectNow = DBConnect.getInstance();
			Connection connection = connectNow.getConnection();

			ResultSet rs = connection.createStatement().executeQuery(" SELECT * FROM holiday WHERE id=1 ");

			if (rs.next()) {
				holiday = new TableHolidays(rs.getString("id"), rs.getString("name"), rs.getDate("departure_date"),
						rs.getInt("duration"), rs.getString("destination"), rs.getString("language"));
			}

			Assert.assertEquals("1", holiday.getId());
			Assert.assertEquals("Viaggio culturale a Roma", holiday.getName());
			Assert.assertEquals("2021-10-24", holiday.getDeparture_date().toString());
			Assert.assertEquals(new BigDecimal(4), new BigDecimal(holiday.getDuration()));
			Assert.assertEquals("Roma (Italia)", holiday.getDestination());
			Assert.assertEquals("Italiano", holiday.getLanguage());

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method test the insert query for the college
	 */
	@Test
	public void testAddCollege() {
		Boolean result = application.control.ControllerAddCollege.setValue("This is a test", "This is a test");
		assertEquals(true, result);
	}

	/**
	 * This method test the insert query for the survey
	 */
	@Test
	public void testAddHostFamily() {
		Boolean result = application.control.ControllerSurvey.setValue(2, "0", "0", "This is a test");
		assertEquals(true, result);
	}

	/**
	 * This method test if the user has been successfully registered
	 * 
	 * @throws NoSuchAlgorithmException
	 */
	@Test
	public void testRegistrationUser() throws NoSuchAlgorithmException {
		Boolean testRegistrationUser = application.control.ControllerRegistrationUser.setValue("Test", "Test", "Test",
				"Test", "Test", "Test", "Test");
		assertEquals(testRegistrationUser, true);
	}

	/***
	 * This method test if the data in the table matches the data in the database
	 */
	@Test
	public void testViewQuestionnarie() {

		TableViewQuestionnaire table = null;

		try {

			DBConnect connectNow = DBConnect.getInstance();
			Connection connection = connectNow.getConnection();

			ResultSet rs = connection.createStatement().executeQuery(" SELECT * FROM survey WHERE id=1 ");

			if (rs.next()) {
				table = new application.control.ControllerUserQuestionnarie.TableViewQuestionnaire(rs.getString("name"),
						rs.getString("grade"), rs.getString("comments"));
			}

			Assert.assertEquals("Viaggio culturale a Roma", table.getName());
			Assert.assertEquals("9", table.getGrade());
			Assert.assertEquals("Esperienza stupenda!", table.getComments());

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}
}
