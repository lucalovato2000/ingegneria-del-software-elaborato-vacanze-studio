module vacanze_studio {

	requires transitive javafx.controls;
	requires javafx.fxml;
	requires transitive java.sql;
	requires transitive javafx.graphics;
	requires javafx.base;
	requires junit;

	opens application to javafx.fxml, javafx.graphics, javafx.TextField, javafx.PasswordField, javafx.base;
	opens application.control to javafx.fxml;
	opens application.model to javafx.fxml;

	exports application;
	exports application.control;
	exports application.model;
}
